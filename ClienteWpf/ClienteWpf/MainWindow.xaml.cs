﻿using Newtonsoft.Json.Linq;
using SimpleTCP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClienteWpf
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        SimpleTcpClient client;

        public MainWindow()
        {
            InitializeComponent();
            CreateTpxClient();
            SearchIps();
        }

        private void CreateTpxClient()
        {
            client = new SimpleTcpClient();
            client.StringEncoder = Encoding.UTF8;
            client.DataReceived += Client_DataReceived;
        }

        private void SearchIps()
        {
            string[] aux = Array.FindAll(Dns.GetHostEntry(string.Empty).AddressList, a => a.AddressFamily == AddressFamily.InterNetwork)[0].ToString().Split('.');
            string ipBase = aux[0] + '.' + aux[1] + '.' + aux[2] + '.';
            for (int i = 0; i < 255; i++)
            {
                string ip = ipBase + i.ToString();
                Ping ping = new Ping();
                ping.PingCompleted += Ping_PingCompleted;
                ping.SendAsync(ip, 10, ip);
            }
        }

        private void Client_DataReceived(object sender, Message e)
        {
            string msg = e.MessageString.Substring(0, e.MessageString.Length - 1);
            txtJson.Dispatcher.Invoke(new Action(() => 
            { 
                txtJson.Text = msg; 
            }));
            JObject json = JObject.Parse(msg);
            foreach (JProperty item in json.Children<JProperty>())
            {
                lbxStats.Dispatcher.Invoke(new Action(() => 
                { 
                    lbxStats.Items.Add(item.Name + " = " + item.Value.ToString()); 
                }));
            }
        }

        private void Ping_PingCompleted(object sender, PingCompletedEventArgs e)
        {
            string ip = (string)e.UserState;
            if (e.Reply != null && e.Reply.Status == IPStatus.Success)
            {
                lbxIps.Items.Add(ip);
            }
        }

        private void lbxIps_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (lbxIps.SelectedItem == null)
            {
                lbxIps.SelectedItem = lbxIps.Items[0];
            }
            Button_Click(null, null);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            lbxStats.Items.Clear();
            Storyboard sb = null;
            try
            {
                client.Connect(lbxIps.SelectedItem.ToString(), 8910);
                client.WriteLineAndGetReply(txtMsg.Text, TimeSpan.FromSeconds(3));
                sb = this.FindResource("WindowOpen") as Storyboard;
            }
            catch (Exception ex)
            {
                lbxStats.Items.Add("Estado = Off");
                sb = this.FindResource("WindowHalfOpen") as Storyboard;
            }
            sb.Begin();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }
    }
}
