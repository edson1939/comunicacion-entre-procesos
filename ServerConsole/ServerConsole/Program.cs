﻿using Newtonsoft.Json.Linq;
using SimpleTCP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace ServerConsole
{
    class Program
    {
        static SimpleTcpServer server;

        static string info;

        static void Main(string[] args)
        {
            
            Console.WriteLine("Server starting...");
            IPAddress[] ipv4Addresses = Array.FindAll(Dns.GetHostEntry(string.Empty).AddressList, a => a.AddressFamily == AddressFamily.InterNetwork);
            string port = "8910";
            server = new SimpleTcpServer();
            server.Delimiter = 0x13;
            server.StringEncoder = Encoding.UTF8;
            server.DataReceived += Server_DataReceived;
            info = BuildPcInfo();
            IPAddress ip = IPAddress.Parse(ipv4Addresses[0].ToString());
            server.Start(ip, Convert.ToInt32(port));
            Console.WriteLine(("Server started on ip: {0} and port: {1}"), ipv4Addresses[0].ToString(), port);
            Console.ReadKey();
            server.Stop();
            Console.WriteLine(Environment.NewLine + "Server Stopped push any button to close");
            Console.ReadKey();
        }

        private static void Server_DataReceived(object sender, Message e)
        {
            Console.WriteLine(("({0}) Message received: {1}"), DateTime.Now.ToString(), e.MessageString.Substring(0, e.MessageString.Length - 1));
            e.ReplyLine(info);
        }

        private static string BuildPcInfo()
        {
            JObject json = new JObject();
            dynamic jsonObject = new JObject();
            jsonObject.Estado = "On";
            jsonObject.Fecha = DateTime.Now;
            jsonObject.Procesador = GetComponent("Win32_Processor", "Name");
            jsonObject.TarjetaVideo = GetComponent("Win32_VideoController", "Name");
            jsonObject.PlacaMadre = GetComponent("Win32_BaseBoard", "Product");
            jsonObject.Ram = GetComponent("Win32_PhysicalMemory", "Capacity");
            json = jsonObject;
            return json.ToString();
        }

        private static string GetComponent(string hwClass, string syntax)
        {
            List<string> res = new List<string>();
            ManagementObjectSearcher mos = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM " + hwClass);
            foreach (var item in mos.Get())
            {
                res.Add(item[syntax].ToString());
            }
            if (res.Count > 1)
            {
                string x = res[0];
                for (int i = 1; i < res.Count; i++)
                {
                    x += " - " + res[i];
                }
                return x;
            }
            else
            {
                return res[0].ToString();
            }
        }
    }
}
